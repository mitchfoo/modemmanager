
SUBDIRS = . tests

noinst_LTLIBRARIES = libmm-generated.la

if WITH_TIME
MM_TIME_DOC = mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Time.xml
MM_TIME_API = $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Time.xml
endif

if WITH_MESSAGING
MM_GDBUS_SMS_C = mm-gdbus-sms.c
MM_GDBUS_SMS_H = mm-gdbus-sms.h
MM_SMS_DOC = mm-gdbus-doc-org.freedesktop.ModemManager1.Sms.xml
MM_MESSAGING_DOC = mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Messaging.xml
MM_MESSAGING_API = $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Messaging.xml
endif

if WITH_LOCATION
MM_LOCATION_DOC = mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Location.xml
MM_LOCATION_API = $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Location.xml
endif

if WITH_FIRMWARE
MM_FIRMWARE_DOC = mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Firmware.xml
MM_FIRMWARE_API = $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Firmware.xml
endif

if WITH_SIGNAL
MM_SIGNAL_DOC = mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Signal.xml
MM_SIGNAL_API = $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Signal.xml
endif

if WITH_USSD
MM_USSD_DOC = mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Modem3gpp.Ussd.xml
MM_USSD_API = $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Modem3gpp.Ussd.xml
MM_USSD_ANNOTATE = --annotate "org.freedesktop.ModemManager1.Modem.Modem3gpp.Ussd" org.gtk.GDBus.C.Name Modem3gppUssd
endif

GENERATED_H = \
	mm-enums-types.h \
	mm-errors-types.h \
	mm-gdbus-manager.h \
	mm-gdbus-sim.h \
	$(MM_GDBUS_SMS_H) \
	mm-gdbus-bearer.h \
	mm-gdbus-modem.h

GENERATED_C = \
	mm-enums-types.c \
	mm-errors-types.c \
	mm-errors-quarks.c \
	mm-gdbus-manager.c \
	mm-gdbus-sim.c \
	$(MM_GDBUS_SMS_C) \
	mm-gdbus-bearer.c \
	mm-gdbus-modem.c

GENERATED_DOC = \
	mm-gdbus-doc-org.freedesktop.ModemManager1.xml \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Sim.xml \
	$(MM_SMS_DOC) \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Bearer.xml \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.xml \
	$(MM_MESSAGING_DOC) \
	$(MM_LOCATION_DOC) \
	$(MM_TIME_DOC) \
	$(MM_FIRMWARE_DOC) \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Oma.xml \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.ModemCdma.xml \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Modem3gpp.xml \
	$(MM_USSD_DOC) \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Simple.xml \
	$(MM_SIGNAL_DOC)

BUILT_SOURCES = $(GENERATED_H) $(GENERATED_C) $(GENERATED_DOC)

# Enum types
mm-enums-types.h: Makefile.am $(top_srcdir)/include/ModemManager-enums.h $(top_srcdir)/build-aux/mm-enums-template.h
	$(AM_V_GEN) $(GLIB_MKENUMS) \
		--fhead "#include <ModemManager.h>\n#ifndef __MM_ENUMS_TYPES_H__\n#define __MM_ENUMS_TYPES_H__\n" \
		--template $(top_srcdir)/build-aux/mm-enums-template.h \
		--ftail "#endif /* __MM_ENUMS_TYPES_H__ */\n" \
		$(top_srcdir)/include/ModemManager-enums.h > $@

mm-enums-types.c: Makefile.am $(top_srcdir)/include/ModemManager-enums.h $(top_srcdir)/build-aux/mm-enums-template.c mm-enums-types.h
	$(AM_V_GEN) $(GLIB_MKENUMS) \
		--fhead "#include \"mm-enums-types.h\"\n" \
		--template $(top_srcdir)/build-aux/mm-enums-template.c \
		$(top_srcdir)/include/ModemManager-enums.h > $@

# Error types & quarks
mm-errors-types.h: Makefile.am $(top_srcdir)/include/ModemManager-errors.h $(top_srcdir)/build-aux/mm-errors-template.h
	$(AM_V_GEN) $(GLIB_MKENUMS) \
		--fhead "#ifndef __MM_ERRORS_TYPES_H__\n#define __MM_ERRORS_TYPES_H__\n" \
		--template $(top_srcdir)/build-aux/mm-errors-template.h \
		--ftail "#endif /* __MM_ERRORS_TYPES_H__ */\n" \
		$(top_srcdir)/include/ModemManager-errors.h > $@

mm-errors-types.c: Makefile.am $(top_srcdir)/include/ModemManager-errors.h $(top_srcdir)/build-aux/mm-errors-template.c mm-errors-types.h
	$(AM_V_GEN) $(GLIB_MKENUMS) \
		--fhead "#include <ModemManager.h>\n#include \"mm-errors-types.h\"\n" \
		--template $(top_srcdir)/build-aux/mm-errors-template.c \
		$(top_srcdir)/include/ModemManager-errors.h > $@

mm-errors-quarks.c: Makefile.am $(top_srcdir)/include/ModemManager-errors.h $(top_srcdir)/build-aux/mm-errors-quarks-template.c $(top_builddir)/include/ModemManager-names.h mm-errors-types.h
	$(AM_V_GEN) $(GLIB_MKENUMS) \
		--fhead "#include <ModemManager.h>\n#include \"mm-errors-types.h\"\n" \
		--template $(top_srcdir)/build-aux/mm-errors-quarks-template.c \
		$(top_srcdir)/include/ModemManager-errors.h > $@

# Manager interface
mm_gdbus_manager_generated = \
	mm-gdbus-manager.h \
	mm-gdbus-manager.c \
	mm-gdbus-doc-org.freedesktop.ModemManager1.xml
$(mm_gdbus_manager_generated): $(top_srcdir)/introspection/org.freedesktop.ModemManager1.xml
	$(AM_V_GEN) gdbus-codegen \
		--interface-prefix org.freedesktop.ModemManager1. \
		--c-namespace=MmGdbus \
		--generate-docbook mm-gdbus-doc \
		--generate-c-code mm-gdbus-manager \
		$< \
		$(NULL)

# Modem interfaces
mm_gdbus_modem_generated = \
	mm-gdbus-modem.h \
	mm-gdbus-modem.c \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.xml \
	$(MM_MESSAGING_DOC) \
	$(MM_LOCATION_DOC) \
	$(MM_TIME_DOC) \
	$(MM_FIRMWARE_DOC) \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Oma.xml \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.ModemCdma.xml \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Modem3gpp.xml \
	$(MM_USSD_DOC) \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Modem.Simple.xml \
	$(MM_SIGNAL_DOC)

mm_gdbus_modem_deps = \
	$(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.xml \
	$(MM_MESSAGING_API) \
	$(MM_LOCATION_API) \
	$(MM_TIME_API) \
	$(MM_FIRMWARE_API) \
	$(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Oma.xml \
	$(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.ModemCdma.xml \
	$(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Modem3gpp.xml \
	$(MM_USSD_API) \
	$(top_srcdir)/introspection/org.freedesktop.ModemManager1.Modem.Simple.xml \
	$(MM_SIGNAL_API)
$(mm_gdbus_modem_generated): $(mm_gdbus_modem_deps)
	$(AM_V_GEN) gdbus-codegen \
		--interface-prefix org.freedesktop.ModemManager1. \
		--c-namespace=MmGdbus \
		--generate-docbook mm-gdbus-doc \
		--generate-c-code mm-gdbus-modem \
		--c-generate-object-manager \
		--annotate "org.freedesktop.ModemManager1.Modem.ModemCdma" org.gtk.GDBus.C.Name ModemCdma \
		--annotate "org.freedesktop.ModemManager1.Modem.Modem3gpp" org.gtk.GDBus.C.Name Modem3gpp \
		$(MM_USSD_ANNOTATE) \
		$^ \
		$(NULL)

# SIM interface
mm_gdbus_sim_generated = \
	mm-gdbus-sim.h \
	mm-gdbus-sim.c \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Sim.xml
$(mm_gdbus_sim_generated): $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Sim.xml
	$(AM_V_GEN) gdbus-codegen \
		--interface-prefix org.freedesktop.ModemManager1. \
		--c-namespace=MmGdbus \
		--generate-docbook mm-gdbus-doc \
		--generate-c-code mm-gdbus-sim \
		$< \
		$(NULL)

# Bearer interface
mm_gdbus_bearer_generated = \
	mm-gdbus-bearer.h \
	mm-gdbus-bearer.c \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Bearer.xml
$(mm_gdbus_bearer_generated): $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Bearer.xml
	$(AM_V_GEN) gdbus-codegen \
		--interface-prefix org.freedesktop.ModemManager1. \
		--c-namespace=MmGdbus \
		--generate-docbook mm-gdbus-doc \
		--generate-c-code mm-gdbus-bearer \
		$< \
		$(NULL)

if WITH_MESSAGING
# SMS interface
mm_gdbus_sms_generated = \
	mm-gdbus-sms.h \
	mm-gdbus-sms.c \
	mm-gdbus-doc-org.freedesktop.ModemManager1.Sms.xml
$(mm_gdbus_sms_generated): $(top_srcdir)/introspection/org.freedesktop.ModemManager1.Sms.xml
	$(AM_V_GEN) gdbus-codegen \
		--interface-prefix org.freedesktop.ModemManager1. \
		--c-namespace=MmGdbus \
		--generate-docbook mm-gdbus-doc \
		--generate-c-code mm-gdbus-sms \
		--annotate "org.freedesktop.ModemManager1.Sms:Data" org.gtk.GDBus.C.ForceGVariant True \
		$< \
		$(NULL)
endif

nodist_libmm_generated_la_SOURCES = \
	$(GENERATED_H) \
	$(GENERATED_C)

libmm_generated_la_CPPFLAGS = \
	$(LIBMM_GLIB_CFLAGS) \
	-I$(top_srcdir) \
	-I$(top_srcdir)/include \
	-I$(top_builddir)/include \
	-Wno-unused-function \
	-Wno-float-equal \
	-Wno-shadow

libmm_generated_la_LIBADD = \
	$(LIBMM_GLIB_LIBS)

includedir = @includedir@/libmm-glib
include_HEADERS = $(GENERATED_H)

CLEANFILES = $(GENERATED_H) $(GENERATED_C) $(GENERATED_DOC)
